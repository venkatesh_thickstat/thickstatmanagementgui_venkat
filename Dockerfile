
FROM thickstatdevops/uibuilder:8.9.1
WORKDIR /workingdir
ADD . /workingdir
RUN npm install && \
    bower --allow-root install && \
    grunt


FROM pierrezemb/gostatic 
# Bundle app source
COPY --from=0 /workingdir/dist/build /srv/http
WORKDIR /
EXPOSE  8043
ENTRYPOINT ["/goStatic"]