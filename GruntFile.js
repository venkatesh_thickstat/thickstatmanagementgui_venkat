module.exports = function(grunt) {

	// Load the plugins from package.json Dependencies
	require('load-grunt-tasks')(grunt);
	// Time how long tasks take. Can help when optimizing build times
	require('time-grunt')(grunt);

	// Project configuration.
	grunt.initConfig({
		pkg : grunt.file.readJSON('package.json'),

		//This takes care of doing static analysis of the java script error to find issues in the code
		jshint : {
			all : {
				options : {
					jshintrc : './.jshintrc',
					reporterOutput: ''
				},
				src : ['src/**/*.js', '!node_modules/**/*.js', '!libs/**/*.js', '!src/app/common/core/base64.js']
			}
		},

		sass : {
			dist : {
				files : {
					'src/resources/css/index.css' : 'src/resources/sass/index.scss'
				}
			}
		},
		watch : {
			source : {
				files : ['src/resources/sass/**/*.scss'],
				tasks : ['sass'],
				options : {
					livereload : true, // needed to run LiveReload
				}
			}
		},


		//This target takes care of building/optimizing the javascript/css files
		requirejs : {
			js : {
				options : {
					baseUrl : "src",
					mainConfigFile : "src/main.js",
					wrap : true,
					//dir : "dist/build",
					name : "../node_modules/almond/almond", // assumes a production build using almond
					findNestedDependencies : true,
					removeCombined : true,
					preserveLicenseComments : false,
					optimize: 'none',
	        // out: function(text, sourceMapText) {
	        //     var UglifyJS = require('uglify-es'),
	        //         uglified = UglifyJS.minify(text);
					//
	        //     grunt.file.write('dist/build/main.min.js', uglified.code);
	        // },
					generateSourceMaps: false,
					include : ["main"],
					//name : "almond",
					out : "dist/build/main.min.js"
					// modules : [{
					// name : "main",
					// exclude : ["infrastructure"]
					// }, {
					// name : "infrastructure"
					// }]
				}
			}

			// css : {
			// 	options : {
			// 		optimizeCss : "standard",
			// 		cssIn : "src/resources/css/index.css",
			// 		out : "dist/build/resources/css/index.css"
			// 	}
			// }
		},

		//Takes care of optimizing the image files
		imagemin : {
			dist : {
				files : [{
					expand : true,
					cwd : 'src/resources/images',
					src : '**/*.{gif,png,jpg,jpeg,svg,ico}',
					dest : 'dist/build/resources/images'
				}]
			}
		},

		//Takes care of optimizing the html files
		htmlmin : {
			dist : {
				options : {
					keepClosingSlash: true,
					removeComments: true,
					collapseWhitespace: true
				},
				files : [{
					expand : true,
					cwd : 'src',
					src : ['**/*.html'],
					dest : 'dist/build'
				}]
			}
		},

		cssmin: {
		  target: {
		    files: [{
		      expand: true,
		      flatten : true,
		      sourceMap: true,
		      cwd: '.',
		      src: 'src/resources/css/index.css',
		      dest: 'dist/build/resources/css',
		      ext: '.min.css'
		    }]
		  }
		},

		//copies any files missed by requirejs target
		copy : {
			materializeFonts : {
				expand : true,
				flatten : true,
				cwd : '.',
				src : 'src/resources/fonts/*',
				dest : 'dist/build/resources/fonts'
			},

			robotofonts : {
				expand : true,
				flatten : true,
				cwd : '.',
				src : 'src/resources/fonts/Roboto/*',
				dest : 'dist/build/resources/fonts/Roboto'
			},

			config: {
				expand : true,
				flatten : true,
				cwd: '.',
				src: "config/*",
				dest: "dist/build/config"
			}
		},

		replace: {
	      material_modal: {
		    src: ['bower_components/materialize/js/modal.js'],
		    overwrite: true,
		    replacements: [{
		      from: 'Vel(',
		      to: "$.Velocity("
		    },{
		    	from: 'Vel.hook',
		    	to: "$.Velocity.hook"
		    }]
		  }
	    },

		//This takes care making neccessary changes in the production version like changing relative paths etc.
		'string-replace' : {
			dist : {
				files : {
					"dist/build/index.html" : "dist/build/index.html",
					"dist/build/main.min.js" : "dist/build/main.min.js"
				},
				options : {
					replacements : [{
						pattern : '<script src="../bower_components/requirejs/require.js" data-main="main"></script>',
						replacement : '<script src="./main.min.js" ></script>'
					}, {
						pattern : '<link rel="stylesheet" href="resources/css/index.css" type="text/css">',
						replacement : '<link rel="stylesheet" href="resources/css/index.min.css" type="text/css">'
					}, {
						pattern : /\.\.\/config\/config.json/gi,
						replacement : 'config/config.json'
					}/*, {
						pattern : /\.\.\/\.\.\/\.\.\/bower_components\/bootstrap\/dist/gi,
						//pattern : /bootstrap/g,
						replacement : '..'
					},{
						pattern : /\.\.\/css\/fonts\/Theinhardt/gi,
						replacement : 'fonts/Theinhardt'
					}*/]
				}
			}
		},

		"jsbeautifier" : {
			"beautify" : {
				src : ["src/**/*.js", "src/**/*.html"],
				options : {
					js : {
						maxPreserveNewlines : 2,
						indentWithTabs : true,
						eol : "\r\n",
						endWithNewline : true
					},
					html: {
			              braceStyle: "collapse",
			              indentChar: " ",
			              indentScripts: "keep",
			              indentSize: 4,
			              maxPreserveNewlines: 10,
			              preserveNewlines: true,
			              unformatted: ["a", "sub", "sup", "b", "i", "u"],
			              wrapLineLength: 0
			          }
				}
			},

			"git-pre-commit" : {
				src : ["src/**/*.js"],
				options : {
					mode : "VERIFY_ONLY",
					js : {
						maxPreserveNewlines : 2,
						indentWithTabs : true,
						eol : "\r\n",
						endWithNewline : true
					}
				}
			}
		},

		githooks : {
			all : {
				// Will run the jshint and test:unit tasks at every commit
				'pre-commit' : 'jshint jsbeautifier:git-pre-commit'
			}
		},

		clean : ['dist']

	});

	grunt.registerTask('cpyMaterialFile', function() {
		grunt.file.copy("./bower_components/materialize/js/modal.js", "./tmp/modal.js");
	});


	grunt.registerTask('done', function() {
		grunt.file.copy("./tmp/modal.js", "./bower_components/materialize/js/modal.js");
		grunt.file.delete("./tmp/");
	});

	// Default task(s).
	grunt.registerTask('default', ['clean', 'cpyMaterialFile', 'sass', 'githooks', 'jshint', 'jsbeautifier:beautify', 'replace', 'requirejs', 'imagemin', 'htmlmin', 'cssmin', 'copy', 'string-replace', 'done']);
	grunt.registerTask('beautifySource', ['jsbeautifier:beautify']);
};
