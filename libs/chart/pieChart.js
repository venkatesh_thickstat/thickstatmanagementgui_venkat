define(["chartjs"], function(Chart){

    'use strict'

    function setColor(dataDim) {
        if (this.type == "line") {
            this.options.data.datasets[0].fill = false
            this.options.data.datasets[0].borderColor = borderColor[0]
        } else if (this.type == "bar") {
            for (i = 0; i < dataDim.length; i++) {
                this.options.data.datasets[0].backgroundColor[i] = backgroundColor[1]
                this.options.data.datasets[0].borderColor[i] = borderColor[1]
            }
        }
    }

    var backgroundColor = [
        "#00849c",
        "#596171",
        "#71de49",
        "#a35b85",
        "#00b09e",
        "#f0292f",
        "#ff8c00",
        "#9bb4ba",
        "#009688"
    ];

    var borderColor = [
        "#00849c",
        "#596171",
        "#71de49",
        "#a35b85",
        "#00b09e",
        "#f0292f",
        "#ff8c00",
        "#9bb4ba",
        "#009688"
    ];

    var pieChart = (chartOption, title, data) => {
        const dimCols = data.dim.cols;
        const metCols = data.met.cols;
        let stackData = {}
        chartOption.data.labels = data.dim.value[dimCols[0]] // Take the first column
        chartOption.data.datasets[0].data = data.met.value[metCols[0]]
        chartOption.data.datasets[0].label = " Need to Be Changed"
        for (let i = 0; i < dimCols[0].length; i++) {
            chartOption.data.datasets[0].backgroundColor[i] = backgroundColor[i]
        }
        chartOption.options = {
          "legend":{
  					display: true,
  					position: 'left',
            fullWidth: true,
            labels: {
              usePointStyle: true,
  						boxWidth: 2,
  						fontFamily: "'RobotoReg'",
  						fontSize: 10,
  						padding: 15
  					}
  				},
          "pieceLabel": {
            render: 'percentage',
            showZero: true,
            overlap: false,
            fontColor: '#FFF',
            precision: 2,
            fontSize: 10,
            position: 'default',
            showActualPercentages: true,
            fontFamily: "'RobotoReg'"
          }
        };
        return chartOption;
    };

    return pieChart;

});
