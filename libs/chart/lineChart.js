define([], function(){
    'use strict'
    //const log = require('../../logger').logger

    var backgroundColor = [
        "#00849c",
        "#596171",
        "#71de49",
        "#a35b85",
        "#00b09e",
        "#ff8c00",
        "#9bb4ba",
        "#f0292f",
        "#009688"
    ];

    var borderColor = [
        "#00849c",
        "#596171",
        "#71de49",
        "#a35b85",
        "#00b09e",
        "#ff8c00",
        "#9bb4ba",
        "#f0292f",
        "#009688"
    ];



    var lineChart = function(chartOption, title, data) {
      const trendLen = data.trend.cols.length
      const trendCols = data.trend.cols
      const metCols = data.met.cols
      console.log(JSON.stringify(data))
      if (trendLen == 1) {
          chartOption.data.datasets = []
          for (let idx in metCols) {
              chartOption.data.datasets.push({})
              let dataset = chartOption.data.datasets[idx]
              dataset.fill = false
              dataset.label = metCols[idx]
              dataset.borderWidth = 3
              dataset.borderColor = borderColor[idx]
              dataset.backgroundColor = borderColor[idx]
              dataset.data = data.met.value[metCols[idx]]
          }
          chartOption.options.scales.xAxes.scaleLabel = {
              display: true,
              labelString: 'Period'
          }
          chartOption.options.scales.yAxes.scaleLabel = {
              display: true,
              labelString: 'Value'
          }
          chartOption.data.labels = data.trend.value[trendCols[0]] // Take the first column
          console.log(JSON.stringify(chartOption))
          return chartOption
      } else if (trendLen == 2) {
          chartOption.data.labels = data.trend.value[trendCols[0]] // Take the first column
          const uniqueValue = new Set(data.trend.value[trendCols[1]])

          uniqueValue.forEach((label) => {
                  stackData[label] = { "label": label, data: [] }
              })
              //Currently works only for 1 metrics and 2 dimension
          const metValue = data.met.value[metCols[0]]
          secondTrendValue = data.trend.value[trendCols[1]]
          for (let row of metValue.entries()) {
              stackData[secondTrendValue[row[0]]].data.push(row[1])
          }
          let colorIdx = 0
          chartOption.data.datasets = []
          for (let key in object.keys(stackData)) {
              stackData[key]["backgroundColor"] = backgroundColor[colorIdx]
              stackData[key]["borderColor"] = borderColor[colorIdx]
              colorIdx = colorIdx + 1
              chartOption.data.datasets.push(stackData[key])
          }
          console.log(chartOption)
          return chartOption
      }

    };

    return lineChart;
});
