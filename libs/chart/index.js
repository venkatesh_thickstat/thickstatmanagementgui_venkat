'use strict'

const ChartOption = require('./options').defaults
const config = require('../../../config')
const uuid = require('uuid')
const log = require('../../logger').logger

class Engine {
    constructor() {}
    async process(context) {
        try {
            log.info("Processing in Chart")
            if (context.dataTransformation.chart == false) {
                return context
            } else {
                const chartOption = new ChartOption()
                const chartData = await chartOption.buildOptions(context.dataTransformation.data, "Title")
                const fileName = uuid().replace("-", "_") + ".png"
                const filePath = config.baseDir + "/chartimages/" + fileName
                if (context.preProcessed.chartOutputType == "file") {
                    let ret = await chartOption.getChartImageFile(chartData, filePath)
                    context["chart"] = { data: chartData, fileName: fileName }
                    return context
                } else {
                    context["chart"] = { data: chartData, fileName: fileName }
                    return context
                }
            }
        } catch (e) {
            log.error(e.stack)
            context["dataTransformation"] = {
                chart: false,
                status: "failed",
                text: "I am not able to process your question for the selected dataset. Rephrase the question or select the right dataset and try again."
            }
            context["skip"] = "response"
            context["errorStack"] = e.stack()
            return context
        }
    }

}
exports.default = Engine