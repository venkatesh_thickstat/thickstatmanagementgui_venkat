'use strict'
var deepcopy = require("deepcopy");
const ChartjsNode = require('chartjs-node');
const lineChart = require('./lineChart');
const barChart = require('./barChart');
const pieChart = require('./pieChart');
var uid = require('uid')
const log = require('../../logger').logger

const chartOptions = {
    "type": "",
    "data": {
        "labels": [],
        "datasets": [{
            "label": "",
            "data": [],
            "backgroundColor": [],
            "borderColor": [],
            "borderWidth": 1
        }]
    },
    "options": {
        "title": {
            "display": true,
            "text": ""
        },
        "scales": {
            "yAxes": [{
                "ticks": {
                    "beginAtZero": false
                },
                "gridLines": {
                    "display": false
                }
            }],
            "xAxes": [{
                "gridLines": {
                    "display": false
                }
            }]
        },
        "chartArea": {
            "backgroundColor": 'rgba(255, 255, 255, 0)'
        }
    },
    plugins: {
        afterDraw: function(chart, easing) {
            if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
                var ctx = chart.chart.ctx;
                var chartArea = chart.chartArea;

                ctx.save();
                ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
                ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
                ctx.restore();
            }
        }
    }
}

class ChartOptions {
    constructor(type, chartWidth = 500, chartHeight = 500) {
        this.type = type
        this.chartOptions = chartOptions
        this.chartNode = new ChartjsNode(chartWidth, chartHeight);

    }

    async getChartImageBuffer(data) {
        try {

            await this.chartNode.drawChart(data)
            let ret = this.chartNode.getImageBuffer('image/png')
            this.chartNode.destroy();
            return ret
        } catch (e) {
            this.chartNode.destroy();
            return ""
        }
    }

    async getChartImageFile(data, filename) {
        try {
            let ret = await this.chartNode.drawChart(data)
            log.debug("FileName:", filename)
            this.chartNode.writeImageToFile('image/png', filename)
            this.chartNode.destroy();
            return ret
        } catch (e) {
            log.error(e)
            log.error(e.stack)
            this.chartNode.destroy();
            return ""
        }
    }

    async buildOptions(data, title) {
        let chartData
        try {
            if (data == null) return {}
            const chartOption = this.chartOptions

            if (data.trend.cols.length > 0) {
                chartOption.type = "line"
                chartData = lineChart(chartOption, title, data)
            }
            if (data.dim.cols.length > 0) {
                if (data.dim.cols.length == 1 && data.met.cols.length == 1) {
                    chartOption.type = "pie"
                    chartData = pieChart(chartOption, title, data)
                } else {
                    chartOption.type = "bar"
                    chartData = barChart(chartOption, title, data)
                }
            }
            return chartData
        } catch (e) {
            log.error(e.stack)
            return "error"
        }
    }
}

exports.defaults = ChartOptions
