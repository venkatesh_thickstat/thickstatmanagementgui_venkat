define([], function(){
    'use strict'


    var backgroundColor = [
        "#00849c",
        "#596171",
        "#a35b85",
        "#00b09e",
        "#ff8c00",
        "#9bb4ba",
        "#71de49",
        "#f0292f",
        "#009688"
    ];

    var borderColor = [
        "#00849c",
        "#596171",
        "#a35b85",
        "#00b09e",
        "#ff8c00",
        "#9bb4ba",
        "#71de49",
        "#f0292f",
        "#009688"
    ];


    function setColor(dataDim) {
        if (this.type == "line") {
            this.options.data.datasets[0].fill = false
            this.options.data.datasets[0].borderColor = borderColor[0]
        } else if (this.type == "bar") {
            for (i = 0; i < dataDim.length; i++) {
                this.options.data.datasets[0].backgroundColor[i] = backgroundColor[1]
                this.options.data.datasets[0].borderColor[i] = borderColor[1]
            }
        }
    };

    const barChart = (chartOption, title, data) => {
        const dimCols = data.dim.cols
        const metCols = data.met.cols
        let stackData = {}
        if (data.dim.cols.length == 1) {
            chartOption.data.labels = data.dim.value[dimCols[0]] // Take the first column
            chartOption.data.datasets[0].data = data.met.value[metCols[0]]
            for (let i = 0; i < dimCols[0].length; i++) {
                chartOption.data.datasets[0].backgroundColor[i] = backgroundColor[1]
                chartOption.data.datasets[0].borderColor[i] = backgroundColor[1]
            }
            return chartOption
        } else {
            chartOption.data.labels = Array.from(new Set(data.dim.value[dimCols[0]])); // Take the first column
            const uniqueValue = new Set(data.dim.value[dimCols[1]])

            uniqueValue.forEach((label) => {
                stackData[label] = { "label": label, data: [] }
            })
            //Currently works only for 1 metrics and 2 dimension

            const metValue = data.met.value[metCols[0]]
            const secondDimValue = data.dim.value[dimCols[1]]
            for (let row of metValue.entries()) {
                stackData[secondDimValue[row[0]]].data.push(row[1])
            }
            let colorIdx = 0
            chartOption.data.datasets = []

            for (let key of Object.keys(stackData)) {
                stackData[key]["backgroundColor"] = backgroundColor[colorIdx]
                stackData[key]["borderColor"] = backgroundColor[colorIdx]
                colorIdx = colorIdx + 1
                chartOption.data.datasets.push(stackData[key])
            }
            return chartOption
        }
    };

    return barChart;
});
