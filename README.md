# README #

Thickstat Admin GUI

### What is this repository for? ###

Admin GUI is a single web page application. 
It is developed with latest web technologies like,

* HTML5, CSS3, SASS
* requirejs
* Angular v1.6.7 
* Angular-ui-router v0.4.2
* Restangular
* Materialize CSS framework

As a developer you must know the above technologies to work.

This application helps to create Organisation, Users, Feeds etc.


### How do I get set up? ###

Run the following commands.

1. npm install
2. npm install -g grunt-cli
3. npm install -g bower
4. bower install
5. grunt

After running the above commands you will get the ** dist ** folder in the root path. You configure this dist path in any of your server like nginx or apache or any.
Inside dist folder you could see ** build ** folder inside that ** index.html ** file will be there and you can serve this file to proceed.

** E.g. **
If you want to configure in nginx,
location ~ ^/ {
	root /home/ubuntu/AdminGUI/dist/build/index.html;
	gzip_static on;
	expires 1y;
	.....
	}


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Subramanian M (subramanianm@thickstat.com)
* Gopinath J (gopinathj@thickstat.com)