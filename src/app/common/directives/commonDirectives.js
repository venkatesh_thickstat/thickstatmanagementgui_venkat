define(['directivesModule', 'underscore', 'angular', 'jquery', 'app/common/core/validation'], function(directivesModule, _, angular, $, Validation) {
	'use strict';

	directivesModule.directive('execOnScrollToTop', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				var fn = scope.$eval(attrs.execOnScrollToTop);
				element.on('scroll', function(e) {
					if (!e.target.scrollTop) {
						scope.$apply(fn);
					}
				});
			}
		};

	});

	directivesModule.directive('onEnterKeyPress', function() {
		return function(scope, element, attrs) {
			element.bind("keydown keypress", function(event) {
				if (event.which === 13) {
					scope.$apply(function() {
						scope.$eval(attrs.onEnterKeyPress);
					});

					event.preventDefault();
				}
			});
		};
	});

	directivesModule.directive('validateForm', ['$compile', function($compile) {
		return {
			restrict: 'A',
			scope: false,
			link: function(scope, element, attrs) {
				var config = Validation.defulatConfig;
				$.extend(config, scope.$eval(attrs.validationRules));
				config.submitHandler = function(form) {
					scope.$eval(attrs.validateForm);
				};
				$(element).validate(config);
			}
		};
	}]);

	directivesModule.directive('tsMenu', ['$document', function($document) {
		return {
			restrict: "EAC",
			link: function(scope, elem, attr) {

				elem.bind('click', function() {
					elem.toggleClass('dropdown-active');
					elem.addClass('active-recent');
				});

				$document.bind('click', function() {
					if (!elem.hasClass('active-recent')) {
						elem.removeClass('dropdown-active');
					}
					elem.removeClass('active-recent');
				});

			}
		};
	}]);

	directivesModule.directive('draggable', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				element.draggable({
					stop: function(event, ui) {
						event.stopPropagation();
					}
				});
			}
		};
	});

});
