define(['serviceModules'], function(serviceModules) {
	'use strict';
	serviceModules.service('restService', ['$q', 'Restangular', function($q, Restangular) {

		this.Login = function(params) {
			return Restangular.one("authenticate").post("", params).then(function(resources) {
				return resources.originalElement;
			}, function(error) {
				return $q.reject(error);
			});
		};

		this.GraphQL = function(params) {
			var graphqlObj = {
				"query": params
			};
			return Restangular.one("").post("", graphqlObj).then(function(resources) {
				return resources.originalElement;
			}, function(error) {
				return $q.reject(error);
			});
		};

		this.customGetURL = function(name, url) {
			return Restangular.oneUrl(name, url).get().then(function(resources) {
				return resources.originalElement;
			}, function(error) {
				return $q.reject(error);
			});
		};

		this.customPostURL = function(name, url, params) {
			return Restangular.oneUrl(name, url).post("", params).then(function(resources) {
				return resources.originalElement;
			}, function(error) {
				return $q.reject(error);
			});
		};

	}]);
});
