define(['serviceModules'], function(serviceModules) {
	'use strict';
	serviceModules.service('authService', ['$q', '$rootScope', '$window', 'restService', 'Restangular', function($q, $rootScope, $window, restService, Restangular) {

		this.login = function(obj, cb) {
			restService.Login(obj).then(function(result) {
				if ((result.errors && result.errors.length > 0)) {
					cb(false, result.errors[0].message);
				} else {
					$rootScope.userRole = result.data.role;
					$window.sessionStorage.auth = JSON.stringify(result);
					if (result.data) {
						var headers = {
							"Authorization": result.data.token
						};
						Restangular.setDefaultHeaders(headers);
						restService.GraphQL('{users(email: "' + obj.email + '") { displayName orgId}}').then(function(userDetails) {
							var auth = JSON.parse($window.sessionStorage.auth);
							auth.data.orgid = userDetails.data.users[0].orgid;
							auth.data.displayName = userDetails.data.users[0].displayName;
							$window.sessionStorage.auth = JSON.stringify(auth);
							$rootScope.orgid = userDetails.data.users[0].orgid;
							cb(true, result.data.uid);
						});
					} else {
						cb(false, "You don't have permission. Please contact administrator");
					}
				}
			}, function(error) {
				cb(false, error);
			});
		};

		this.isAuthenticated = function() {
			var userInfo = $window.sessionStorage.auth ? JSON.parse($window.sessionStorage.auth) : {};
			if (userInfo.data && userInfo.data.authentication === "success") {
				return true;
			}
			return false;
		};

		this.logout = function() {
			delete $window.sessionStorage.auth;
			Restangular.setDefaultHeaders({});
		};

	}]);
});
