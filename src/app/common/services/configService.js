/* globals location */ //location variable usage is fine in this file so relaxing the jshint check here
define(['serviceModules'], function(servicesModule) {
	'use strict';
	servicesModule.service('configService', ['$http',
		function($http) {

			var defaultProtocol = "https:",
				defaultHost = "",
				botServerHost = "",
				defaultPort = "",
				appURL = "";

			/**
			 *  Returns the application API URL
			 *
			 * @returns {string}
			 * @private
			 */
			this.getApiURL = function() {
				return appURL + "/api/v1";
			};

			/**
			 * TODO
			 * It return the Application admin URL to access the application components health status
			 *
			 * @returns {string}
			 * @private
			 */
			this.getAppURL = function() {
				return appURL;
			};

			this.getBotServerURL = function() {
				return defaultProtocol + "//" + botServerHost + ":" + defaultPort;
			};

			this.initialize = function() {

				return $http.get('../config/config.json').then(function(config) {

					defaultHost = config.data.apiServerHost;
					botServerHost = config.data.botServerHost;
					defaultPort = config.data.port;

					appURL = defaultProtocol + "//" + defaultHost + ":" + defaultPort;
					return;

				}, function(error) {
					throw new Error("Configuration Error: " + JSON.stringify(error));
				});
			};

			/*,
			 _intitiaze = function() {
			 //initializing the URL from the browser address bar
			 _initializeURL();
			 };*/
		}
	]);
});
