define(['jquery', 'jqValidate'], function($, jqValidate) {
	'use strict';

	(function() {
		//Add custom methods..
		/*$.validator.addMethod("domain", function(value, element) {
		  return this.optional(element) || /^http:\/\/mycorporatedomain.com/.test(value);
		}, "Please specify the correct domain for your documents");*/

	})();

	return {
		defulatConfig: {
			errorElement: 'div',
			errorPlacement: function(error, element) {
				var placement = $(element).data('error');
				if (placement) {
					$(placement).append(error);
				} else {
					error.insertAfter(element);
				}
			}
		}
	};
});
