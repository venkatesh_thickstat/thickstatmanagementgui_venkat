/*  Author Subramanian 
    Created at 27-Jan-2018
    Purpose of this library is to handle all the notification and expose single api
*/
'use strict';
define(["jquery", "Noty"],
	function($, Noty) {

		var notify = function() {

			Noty.overrideDefaults({
				layout: 'topCenter',
				theme: 'sunset',
				animation: {
					open: 'animated bounceInDown', // or Animate.css class names like: 'animated bounceInLeft'
					close: 'animated bounceOutUp', // or Animate.css class names like: 'animated bounceOutLeft'
				},
				closeWith: ['click', 'button'],
				progressBar: true,
				timeout: 1000
			});

			this.show = function(msgObj) {
				if (!msgObj.text) {
					throw new Error("Notification text must contain any message can't be empty.");
				}
				new Noty(msgObj).show();
			};

			this.closeAll = function() {
				Noty.closeAll();
			};

		};

		return notify;

	});
