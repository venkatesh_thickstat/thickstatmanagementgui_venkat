'use strict';
define(['angular', 'app/common/core/tsNotification', 'controllers', 'services', 'directives'], function(angular, Notification) {

	var app = angular.module("tsApp", ['ui.router', 'controllerModules', 'serviceModules', 'directivesModule', 'restangular', 'blockUI', 'ngDomToPdf', 'ngMaterial', 'mdDataTable']);
	var notification = new Notification();

	//This is to verify the authenciation status before changing the application state
	var verifyAuth = ['$q', '$log', 'authService', '$state', function($q, $log, authService, $state) {
		var authenticated = authService.isAuthenticated();
		if (authenticated) {
			return $q.when(authenticated);
		} else {
			$log.error("Routing is not allowed due to authentication failure");
			authService.logout();
			$state.go("login");
			$q.reject({
				authstatus: false
			});
		}
	}];

	app.controller("indexController", ['$scope', function($scope) {
		//Waves.displayEffect(); // jshint ignore:line
		$scope.isAuthenticated = true;
	}]);

	app.run(['configService', '$log', 'Restangular', '$rootScope', '$state', '$window',
		function(configService, $log, Restangular, $rootScope, $state, $window) {
			//Initialize config service
			configService.initialize().then(function() {
				//Initializing Restangular with base URL
				Restangular.setBaseUrl(configService.getApiURL());
				$log.info("Base URL has been set as :" + configService.getApiURL());
			});

			$rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
				return $state.go("login");
			});

			if ($window.sessionStorage.auth) {
				var auth = JSON.parse(($window.sessionStorage.auth));

				$rootScope.userRole = auth.data.role;
				$rootScope.orgid = auth.data.orgid;
				var headers = {
					"Authorization": auth.data.token
				};
				Restangular.setDefaultHeaders(headers);
			}

			//Configure restangular to get the original response
			//This is done to avoid restangular intelligence scattered across the application
			//The service layer should take care of returning the original element always
			Restangular.setResponseExtractor(function(response) {
				var newResponse = response;
				if (newResponse) {
					if (angular.isArray(response)) {
						newResponse.originalElement = [];
						angular.forEach(newResponse, function(value, key) {
							newResponse.originalElement[key] = angular.copy(value);
						});
					} else {
						newResponse.originalElement = angular.copy(response);
					}
				}
				return newResponse;
			});

			//Hook up error interceptor to catch all the API errors at one place.
			Restangular.setErrorInterceptor(function(response) {
				if (response.status === -1 && response.config.url.indexOf('history') > -1) {
					notification.show({
						text: "Unable to fetch history. Please try again later.",
						type: "error",
						timeout: 5000
					});
				}
				return true;
			});
		}
	]);

	//Setting the route configuration based on the state, using the ui-router
	app.config(["$stateProvider", "$urlRouterProvider", "blockUIConfig",
		function($stateProvider, $urlRouterProvider, blockUIConfig) {

			blockUIConfig.delay = 0;
			blockUIConfig.autoBlock = false;
			blockUIConfig.template = '<div class="block-ui-overlay"></div><div class="block-ui-message-container">' +
				'<div class="" ng-class="$_blockUiMessageClass"><div><i class="loading"></i></div>' +
				'<div class="preloader-wrapper big active"><div class="spinner-layer spinner-green-only">' +
				'<div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch">' +
				'<div class="circle"></div></div><div class="circle-clipper right"><div class="circle">' +
				'</div></div></div></div></div></div>';

			//setting the router configuration
			$urlRouterProvider.otherwise("/login");

			$stateProvider.state("login", {
				url: "/login",
				views: {
					"login": {
						templateUrl: "app/login/login.html",
						controller: "loginController"
					}
				}
			}).state("layout", {
				abstract: true,
				views: {
					"layout": {
						templateUrl: "app/layout/layout.html",
						controller: "layoutController"
					}
				}
			}).state("dashboard", {
				url: "/dashboard",
				views: {
					'': {
						templateUrl: "app/dashboard/dashboard.html",
						controller: "dashboardController"
					},
					'user@dashboard': {
						templateUrl: "app/dashboard/user/dashboard.html",
						controller: "userDashboardController"
					}
				},
				parent: 'layout',
				activeTab: 'home',
				resolve: {
					auth: verifyAuth
				}
			});
		}
	]);

	return app;
});
