define(['controllerModules', 'angular', 'jquery'], function(controllerModules, angular, $) {
	'use strict';
	controllerModules.controller("layoutController", ['$scope', '$state', '$window', 'authService',
		function($scope, $state, $window, authService) {

			$scope.loggedUserName = JSON.parse($window.sessionStorage.auth).data.displayName.replace(/\w\S*/g, function(txt) {
				return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
			});

			$scope.userRole = JSON.parse($window.sessionStorage.auth).data.role;

			//$(".dropdown-button").dropdown();

			$scope.$on("selectedDataset", function(evnt, data) {
				$scope.selectedDataset = data;
			});

			$scope.logout = function() {
				authService.logout();
			};

		}
	]);
});
