define(['controllerModules', 'angular', 'numeral', 'jquery', 'underscore', 'chartjs', 'app/common/core/base64', 'jquery', 'app/common/core/tsNotification'], function(controllerModules, angular, numeral, $, _, Chart, Base64, Notification) {
	'use strict';
	controllerModules.controller("userDashboardController", ['$q', '$log', '$rootScope', '$scope', '$mdDialog', '$mdMedia', '$compile', '$location', '$state', '$filter', '$timeout', '$window', 'configService', 'restService', 'userDashboardService', 'blockUI', '$anchorScroll', 'commonUtilityService',
		function($q, $log, $rootScope, $scope, $mdDialog, $mdMedia, $compile, $location, $state, $filter, $timeout, $window, configService, restService, userDashboardService, blockUI, $anchorScroll, commonUtilityService) {

			var notification = new Notification();
			var token = JSON.parse($window.sessionStorage.auth).data.token;
			var DOM_INDEX = 0;
			var _eventEmitter = function() {
				$scope.$emit("selectedDataset", $scope.selectedDataset);
			};
			var historyCountLimit = 10;
			var fetchedHistoryCount = 0;
			var isPrepend = true;
			var isReachedAllHistorys = false;
			var tmpHistoryDate = null;
			var lastQueryDate = null;
			var originalDataWithMultipleFormat = {
				raw: {},
				csv: {},
				table: {},
				chart: {}
			};
			$scope.datasets = [];
			$scope.selectedDataset = {};

			var DialogController = ['$scope', '$mdDialog', function($scope, $mdDialog) {

				$scope.hide = function() {
					$mdDialog.hide();
				};

				$scope.cancel = function() {
					$mdDialog.cancel();
				};

				$scope.answer = function(answer) {
					$mdDialog.hide(answer);
				};
			}];

			$scope.showDetailPopup = function(ev, chartId) {
				var options;
				$mdDialog.show({
					controller: DialogController,
					templateUrl: 'app/dashboard/user/popupTemplate.html',
					parent: angular.element(document.body),
					targetEvent: ev,
					fullscreen: $mdMedia('xs') || $mdMedia('sm'),
					onShowing: function(scope, element) {
						blockUI.start();
						var _cloneResult = JSON.parse(JSON.stringify(originalDataWithMultipleFormat.raw[chartId]));
						options = userDashboardService.buildOptions(_cloneResult.data, "Chart Title");
						scope.mdColumns = originalDataWithMultipleFormat.table[chartId].columns;
						scope.tableData = originalDataWithMultipleFormat.table[chartId].data;
						scope.mdRow = {
							'column-keys': originalDataWithMultipleFormat.csv[chartId][0]
						};
						scope.title = _cloneResult.utterence;
						scope.chartId = chartId;
						scope.exportTable = function() {
							$scope.exportCSV(chartId);
						};
					},
					onComplete: function(scope, element) {
						var chartView = element.find('form div#chart-view');
						chartView.empty();
						var canvas = document.createElement('canvas');
						chartView.append(canvas);
						var ctx = canvas.getContext('2d');
						new Chart(ctx, options);
						scope.$applyAsync(function() {
							//scope.tableData = originalDataWithMultipleFormat.csv[chartId];
							blockUI.stop();
						});

					},
					clickOutsideToClose: true
				}).then(function(answer) {
					$scope.status = 'You said the information was "' + answer + '".';
				}, function() {
					$scope.status = 'You cancelled the dialog.';
				});
			};

			$scope.chatDateTime = new Date();

			//All DOM element variables
			var $MessageBox = $("#mesgBox");
			var $SendBtn = $(".send-btn");
			var $ChatText = $(".chat-text");

			var questionTemplate = '<li class="right"><span class="chat-img pull-right margin-left-10"><i class="material-icons small grey-text">account_circle</i></span>' +
				'<div class="chat-bubble me right"><div class="my-mouth"></div><div class="content">${chatMsg}</div><div class="time">${time}</div>' +
				'</div></li>';

			var answerTemplateWithChart = '<li class="ans-content left"><span class="chat-img pull-left margin-right-15 margin-left-15"><div class="avatar-circle darken">${userName.charAt(0).toUpperCase()}</div></span>' +
				'<div class="chat-bubble athena left"><div class="athena-mouth"></div><div class="settings"><i class="material-icons modal-trigger grey-text margin-right-10" dom-id="${domId}" ng-click="showDetailPopup($event, \'${domId}\')">visibility</i><span class="ts-menu"><i class="material-icons grey-text">file_download</i><span>' +
				'<ul><li pdf-download-button pdf-name="\'Thickstat-Recommendations.pdf\'" pdf-element-id="element-to-download-${domId}" creating-pdf-class-name="\'generating\'">Export to PDF</li>' +
				'<li ng-click="exportCSV(\'${domId}\')">Export to CSV</li></ul>' +
				'</div><div class="content" pdf-content="element-to-download-${domId}"><canvas id="${domId}"></canvas></div><div class="time">${time}</div>' +
				'</div></li>';

			var answerTemplateWithText = '<li class="ans-content left"><span class="chat-img pull-left margin-right-15 margin-left-15"><div class="avatar-circle darken">${userName.charAt(0).toUpperCase()}</div></span>' +
				'<div class="chat-bubble athena left"><div class="athena-mouth"></div><div class="content" id="${domId}"></div><div class="time">${time}</div>' +
				'</div></li>';

			var inlineDateTemplate = '<li id="chat-date"><span>${date}</span></li>';

			//jQuery Template caching
			$.template("quesTemplate", questionTemplate);
			$.template("ansTemplateChart", answerTemplateWithChart);
			$.template("ansTemplateText", answerTemplateWithText);
			$.template("inlineDate", inlineDateTemplate);

			var _question = function(data) {
				var append = isPrepend ? "prependTo" : "appendTo";
				$compile($.tmpl("quesTemplate", data))($scope)[append]("#chatRoom");
			};

			var _answerWithText = function(data) {
				var append = isPrepend ? "prependTo" : "appendTo";
				$compile($.tmpl("ansTemplateText", data))($scope)[append]("#chatRoom");
			};

			var _answerWithChart = function(data) {
				var append = isPrepend ? "prependTo" : "appendTo";
				$compile($.tmpl("ansTemplateChart", data))($scope)[append]("#chatRoom");
			};

			var _convertToDateOnly = function(date) {
				return $filter('date')(date, 'longDate');
			};

			var _convertToTimeOnly = function(date) {
				return $filter('date')(date, 'mediumTime');
			};

			var _setDate = function(date) {
				var append = isPrepend ? "prependTo" : "appendTo";
				var todayDate = _convertToDateOnly(new Date());
				if (date === todayDate) {
					date = "Today";
				}
				$compile($.tmpl("inlineDate", {
					date: date
				}))($scope)[append]("#chatRoom");
			};

			var _makeTimeoutCall = function(fn, data, timeout) {
				$timeout(function() {
					fn.call(null, data);
				}, timeout);
			};

			var _autoScrollContent = function(hash) {
				$ChatText.animate({
					scrollTop: $ChatText.prop("scrollHeight")
				}, 500);
			};

			var _questionFromUser = function(text, isHistory) {
				var date = isHistory ? new Date(isHistory.createdAt) : new Date();
				var timeDisplay = _convertToTimeOnly(date);
				_question({
					userName: $scope.loggedUserName,
					time: timeDisplay,
					chatMsg: text
				});
			};

			var _answerFromServer = function(data, isHistory) {
				DOM_INDEX = DOM_INDEX + 1;
				var date = isHistory ? new Date(isHistory.createdAt) : new Date();
				var timeDisplay = _convertToTimeOnly(date);
				var DOMID = "answerContent_" + DOM_INDEX;
				originalDataWithMultipleFormat.raw[DOMID] = JSON.parse(JSON.stringify(data));
				if (data.chart) {
					var cloneData = JSON.parse(JSON.stringify(data.data));
					originalDataWithMultipleFormat.csv[DOMID] = userDashboardService.formatedDataCSV(cloneData);
					originalDataWithMultipleFormat.table[DOMID] = userDashboardService.formatedDataTable(cloneData);

					_answerWithChart({
						userName: $scope.selectedDataset.domainName,
						time: timeDisplay,
						domId: DOMID
					});

					var options = userDashboardService.buildOptions(data.data, "Chart Title");
					// originalDataWithMultipleFormat.chart[DOMID] = options;
					$scope.$applyAsync(function() {
						var canvasDOM = $("#" + DOMID);
						canvasDOM.parent().parent().attr("style", "width: 70%;");
						var ctx = canvasDOM[0].getContext('2d');
						new Chart(ctx, options);
						$scope.typingIndicator = false;
					});
				} else {
					_answerWithText({
						userName: $scope.selectedDataset.domainName,
						time: timeDisplay,
						domId: DOMID
					});
					$scope.$applyAsync(function() {
						var chartDOM = $("#" + DOMID);
						var contentText = data.text || data.data || "Not Found";
						$(chartDOM).text(contentText);
						$scope.typingIndicator = false;
					});
				}
				if (!isHistory)
					_autoScrollContent();
			};

			var _answerFromServerHistory = function(ansData) {
				var formatData = {
					chart: ansData.chart,
					data: "",
					text: ansData.text,
					utterence: ansData.utterence
				};
				formatData.data = ansData.data ? JSON.parse(Base64.decode(ansData.data)) : "I am not able to process your question for the selected dataset Rephrase the question or select the right dataset and try again";
				_answerFromServer(formatData, {
					createdAt: ansData.createdAt,
					domain: ansData.domain
				});
			};

			var _questionFromUserHistory = function(ansData) {
				_questionFromUser(ansData.utterence, {
					createdAt: ansData.createdAt
				});
			};

			var _getChatHistory = function() {
				isPrepend = true;

				var reqObject = {
					"dataSet": $scope.selectedDataset.id,
					"page": {
						"from": fetchedHistoryCount,
						"size": historyCountLimit
					}
				};
				blockUI.start();
				restService.customPostURL('historys', configService.getBotServerURL() + '/api/historys?token=' + token, reqObject).then(function(chatHistory) {
					isReachedAllHistorys = (chatHistory.data.length !== historyCountLimit);
					if (chatHistory.data.length > 0) {
						_.each(chatHistory.data, function(data) {
							//Create history data with Closure mechanism
							_makeTimeoutCall(
								function(ansData) {
									var historyDate = new Date(ansData.createdAt);
									historyDate = _convertToDateOnly(historyDate);
									if (!tmpHistoryDate && !lastQueryDate) {
										tmpHistoryDate = historyDate;
										//Store the last query date for future reference
										lastQueryDate = historyDate;
									} else if (tmpHistoryDate !== historyDate) {
										_setDate(tmpHistoryDate);
										tmpHistoryDate = historyDate;
									}

									_answerFromServerHistory(ansData);
									_questionFromUserHistory(ansData);
								},
								data,
								0
							);
							$MessageBox.focus();
						});
						$timeout(function() {
							if (fetchedHistoryCount === 0) {
								_autoScrollContent();
							} else {
								$ChatText.animate({
									scrollTop: parseInt($ChatText.prop("clientHeight") * 2)
								}, 100);
							}
							var today = _convertToDateOnly(new Date());
							if (isReachedAllHistorys && tmpHistoryDate === today) {
								_setDate(today);
							} else if (isReachedAllHistorys) {
								_setDate(tmpHistoryDate);
							}
							blockUI.stop();
						}, 0);

					} else {
						blockUI.stop();
					}
				}, function(error) {
					blockUI.stop();
					notification.show({
						text: JSON.stringify(error),
						type: "error"
					});
				});
			};

			$scope.onClickDataset = function(dataSet) {
				$scope.selectedDataset = dataSet;
				_eventEmitter();
				$("#chatRoom").empty();
				fetchedHistoryCount = 0;
				tmpHistoryDate = null;
				lastQueryDate = null;
				_getChatHistory();
				$MessageBox.val("");
				$MessageBox.focus();
			};

			var _fetchUserDataset = function() {
				blockUI.start();
				restService.GraphQL('{userDataSet { id orgId datasetName domainName}}').then(function(userDataSet) {
					var datasets = userDataSet.data.userDataSet;
					$scope.$applyAsync(function() {
						$scope.datasets = datasets;
						$scope.selectedDataset = $scope.datasets[0];
						_eventEmitter();
						blockUI.stop();
						_getChatHistory();
					});
				}, function(error) {
					blockUI.stop();
					notification.show({
						text: JSON.stringify(error),
						type: "error"
					});
				});
			};

			var _getConverseData = function(selectedText) {
				$scope.typingIndicator = true;
				var reqObject = {
					session: {
						message: {
							text: selectedText,
							domain: $scope.selectedDataset.domainName,
							dataSet: $scope.selectedDataset.id
						},
						options: {
							transform: true,
							channel: "chat",
							source: "web"
						}
					}
				};

				restService.customPostURL('converse', configService.getBotServerURL() + '/api/converse?token=' + token, reqObject).then(function(dataSet) {
					$log.info(dataSet.response);
					var data;
					if (dataSet.status === "ok") {
						data = dataSet.response.data;
					} else {
						data = dataSet.response;
					}
					data.utterence = selectedText;
					_answerFromServer(data);

				}, function(err) {
					if (err.status === -1) {
						var errMsg = "Unable to connect a server, Please try again after sometimes!!";
						_answerFromServer(errMsg);
					} else {
						_answerFromServer(JSON.stringify(err));
					}
				});
			};

			//On click -> export to CSV menu item
			$scope.exportCSV = function(exportCSV) {
				commonUtilityService.exportToCsv("AthenaData.csv", originalDataWithMultipleFormat.csv[exportCSV]);
			};

			//Handling scroll top reached
			$scope.handleScrollToTop = function() {
				if (!isReachedAllHistorys && tmpHistoryDate !== null) {
					fetchedHistoryCount = fetchedHistoryCount + historyCountLimit;
					isPrepend = true;
					_getChatHistory();
				} else {
					return;
				}
			};

			var _activateAutoComplete = function() {
				$MessageBox.easyAutocomplete({
					url: function() {
						return configService.getBotServerURL() + '/api/suggestions?token=' + token;
					},
					getValue: function(data) {
						return data.text;
					},

					ajaxSettings: {
						contentType: "application/json",
						crossDomain: true,
						type: "POST",
						data: {
							suggest: {}
						}
					},
					preparePostData: function(data) {
						data.suggest.text = $MessageBox.val();
						data.suggest.dataSet = $scope.selectedDataset.id;
						data.suggest.domain = $scope.selectedDataset.domainName;
						data.suggest.orgId = $scope.selectedDataset.orgId;
						return JSON.stringify(data);
					},
					listLocation: 'data',
					list: {
						match: {
							enabled: true
						},
						onChooseEvent: function(e) {
							//this.maxNumberOfElements = 0;
							//e.stopPropagation();
							$MessageBox.focus();
							// var selectedText = $MessageBox.getSelectedItemData().text;
							// _getConverseData(selectedText);
							// _questionFromUser(selectedText);

						}
					}
				});
			};

			$(document).keypress(function(e) {
				if ($MessageBox.val() !== "" && e.which === 13) {
					$scope.sendMsg();
				}
			});

			$scope.sendMsg = function() {
				if ($MessageBox.val() !== '') {
					var todayDate = _convertToDateOnly(new Date());
					if (lastQueryDate !== todayDate) {
						lastQueryDate = todayDate;
						_setDate(todayDate);
					}
					isPrepend = false;
					_getConverseData($MessageBox.val());
					_questionFromUser($MessageBox.val());

					_autoScrollContent();

					$MessageBox.easyAutocomplete({
						data: []
					});
					_activateAutoComplete();
					$MessageBox.val("");
					$MessageBox.focus();
					// $compile($MessageBox)($scope);
				} else {
					return false;
				}
			};
			//Init (after page load make default function calls)
			_fetchUserDataset();
			_activateAutoComplete();

			//Chart Custom Y-axis Scale
			Chart.scaleService.updateScaleDefaults('linear', {
				ticks: {
					callback: function(value, index, values) {
						return numeral(value).format("0a");
					}
				}
			});

			//Chart Custom Tooltips
			Chart.defaults.global.tooltips.callbacks.label = function(tooltipItem, data) {
				return numeral(tooltipItem.yLabel).format("0,0");
			};
		}

	]);
});
