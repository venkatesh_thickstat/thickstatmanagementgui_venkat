define(['serviceModules', 'underscore', '../../../../libs/chart/lineChart', '../../../../libs/chart/pieChart', '../../../../libs/chart/barChart', 'numeral'], function(serviceModules, _, lineChart, pieChart, barChart, numeral) {
	'use strict';
	serviceModules.service('userDashboardService', ['$q', '$log', function($q, $log) {

		var defaultChartOptions = {
			"type": "",
			"data": {
				"labels": [],
				"datasets": [{
					"label": "",
					"data": [],
					"backgroundColor": [],
					"borderColor": [],
					"borderWidth": 1
				}]
			},
			"options": {
				layout: {
					padding: {
						left: 0,
						right: 10,
						top: 0,
						bottom: 0
					}
				},
				"legend": {
					display: true,
					position: 'bottom',
					fullWidth: true,
					labels: {
						usePointStyle: true,
						boxWidth: 2,
						fontFamily: "'RobotoReg'",
						fontSize: 10,
						padding: 15
					}
				},
				"title": {
					"display": true,
					"text": ""
				},
				"scales": {
					"yAxes": [{
						"ticks": {
							"beginAtZero": false,
							fontFamily: "'RobotoReg'",
							fontSize: 10
						},
						"gridLines": {
							"display": false
						}
					}],
					"xAxes": [{
						barPercentage: 0.95,
						categoryPercentage: 1,
						"ticks": {
							fontFamily: "'RobotoReg'",
							fontSize: 10
						},
						"gridLines": {
							"display": false
						}
					}]
				},
				"chartArea": {
					"backgroundColor": 'rgba(255, 255, 255, 0)'
				}
			}
		};

		this.formatedDataCSV = function(inputData) {
			var tmpColumns = [],
				tmpValues = [];
			_.each(inputData, function(item) {
				tmpColumns.push(_.keys(item.value));
				_.each(item.value, function(subItem, key) {
					_.each(subItem, function(value, index) {
						if (typeof tmpValues[index] === "object") {
							tmpValues[index].push(value);
						} else {
							tmpValues.push([value]);
						}
					});
				});
			});
			tmpColumns = _.flatten(tmpColumns);
			tmpValues.unshift(tmpColumns);
			return tmpValues;
		};

		this.formatedDataTable = function(inputData) {
			var tableTdData = [];
			var tableHeaderColumns = {};
			var finalOutput = {};
			_.each(inputData, function(item) {
				_.each(item.value, function(subItem, key) {
					_.each(subItem, function(value, index) {
						tableHeaderColumns[key] = _.isNumber(value) ? "right" : "left";
						value = _.isNumber(value) ? numeral(value).format('0.00 a') : value;
						if (typeof tableTdData[index] === "object") {
							tableTdData[index][key] = value;
						} else {
							var obj = {};
							obj[key] = value;
							tableTdData.push(obj);
						}
					});
				});
			});
			finalOutput.data = tableTdData;
			finalOutput.columns = tableHeaderColumns;
			return finalOutput;
		};

		this.buildOptions = function(data, title, chartType) {
			let chartData;
			try {
				if (data == null) return {};
				const chartOption = JSON.parse(JSON.stringify(defaultChartOptions));
				if (data.trend.cols.length > 0) {
					if (data.dim.cols.length === 0) {
						chartOption.type = "line";
						chartData = lineChart(chartOption, title, data);
					} else {
						chartOption.type = "bar";
						data.dim.cols = data.trend.cols.concat(data.dim.cols);
						chartData = barChart(chartOption, title, data);
					}
				}
				if (data.dim.cols.length > 0) {
					if (data.dim.cols.length === 1 && data.met.cols.length === 1 && data.met.value[data.met.cols[0]].length <= 10) {
						chartOption.type = "pie";
						chartData = pieChart(chartOption, title, data);
					} else {
						chartOption.type = "bar";
						chartData = barChart(chartOption, title, data);
					}
				}
				return chartData;
			} catch (e) {
				return e;
			}
		};
	}]);
});
