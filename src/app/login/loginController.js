define(['controllerModules', 'angular', 'jquery', 'app/common/core/tsNotification'], function(controllerModules, angular, $, Notification) {
	'use strict';
	controllerModules.controller("loginController", ['$scope', '$rootScope', '$window', '$state', '$log', 'authService', 'restService',
		function($scope, $rootScope, $window, $state, $log, authService, restService) {

			var notification = new Notification();

			$scope.email = "";
			$scope.pass = "";

			var loginResponseHandler = function(isAuthenticated, error) {
				if (isAuthenticated) {
					$rootScope.loggedIn = true;
					$state.go('dashboard');
				} else {
					$scope.$applyAsync(function() {
						$rootScope.loggedIn = false;
						$scope.loginPressed = false;
						if (error.status === -1)
							error = "Unable to connect server. Please try again!";
						notification.show({
							text: error,
							type: "error",
							timeout: 0
						});
					});
				}

			};

			$scope.login = function(form) {
				$scope.loginPressed = true;
				var obj = {
					email: $scope.email,
					password: $scope.pass,
					deviceId: "123456",
					deviceName: "BrowserWeb"
				};
				authService.login(obj, loginResponseHandler);
				notification.closeAll();
			};
		}
	]);
});
