/*global document*/
require.config({
	// baseUrl : '../src',
	// urlArgs : 'v=1.0',
	waitSeconds: 200,
	wrapShim: true,
	paths: {
		//Third party library paths
		'jquery.min': "../bower_components/jquery/dist/jquery.min",
		jqueryUI: "../bower_components/jquery-ui/jquery-ui.min",
		angular: "../bower_components/angular/angular.min",
		angularMaterial: "../bower_components/angular-material/angular-material.min",
		angularAria: "../bower_components/angular-aria/angular-aria.min",
		angularMsg: "../bower_components/angular-messages/angular-messages.min",
		ngAnimate: "../bower_components/angular-animate/angular-animate.min",
		ngSanitize: "../bower_components/angular-sanitize/angular-sanitize.min",
		ngMdIcons: "../bower_components/angular-material-icons/angular-material-icons.min",
		mdTable: "../bower_components/md-data-table/dist/md-data-table",
		mdTableTemplate: "../bower_components/md-data-table/dist/md-data-table-templates",
		uirouter: "../bower_components/angular-ui-router/release/angular-ui-router.min",
		underscore: "../bower_components/underscore/underscore-min",
		Restangular: "../bower_components/restangular/dist/restangular.min",
		chartjs: "../bower_components/chart.js/dist/Chart.bundle.min",
		pieceLabel: "../bower_components/Chart.PieceLabel.js/build/Chart.PieceLabel.min",
		jqueryTemplate: "../libs/jquery.tmpl.min",
		numeral: "../node_modules/numeral/min/numeral.min",
		domToPdf: "../bower_components/ng-dom-to-pdf/dist/ng-dom-to-pdf.min",
		domToImg: "../bower_components/dom-to-image/dist/dom-to-image.min",
		pdfmake: "../bower_components/pdfmake/build/pdfmake.min",
		es6Promise: "../bower_components/es6-promise/es6-promise.min",
		autoComplete: "../bower_components/EasyAutocomplete/dist/jquery.easy-autocomplete.min",
		jqValidate: '../bower_components/jquery-validation/dist/jquery.validate.min',
		Noty: "../bower_components/noty/lib/noty.min",
		lodash: "../bower_components/lodash/dist/lodash.min",
		uiBlock: "../bower_components/angular-block-ui/dist/angular-block-ui.min",
		controllers: "app/common/controllers",
		controllerModules: "app/common/controllerModules",
		services: "app/common/services",
		serviceModules: "app/common/serviceModules",
		directivesModule: "app/common/directivesModule",
		directives: "app/common/directives"
	},

	shim: {

		'jquery.min': {
			exports: 'jQuery'
		},

		jqueryUI: {
			deps: ['jquery.min']
		},

		jqValidate: {
			deps: ['jquery.min']
		},

		Noty: {
			deps: ['jquery.min']
		},

		angular: {
			exports: 'angular',
			deps: ['jquery.min', 'jqueryUI']
		},

		ngSanitize: {
			deps: ['angular'],
		},

		numeral: {
			exports: "numeral"
		},

		ngMdIcons: {
			deps: ['angular'],
		},

		mdTable: {
			deps: ['jquery.min', 'angular', 'ngAnimate', 'ngSanitize', 'ngMdIcons', 'mdTableTemplate']
		},

		mdTableTemplate: {
			deps: ['angular', 'ngMdIcons']
		},

		ngAnimate: {
			deps: ['angular']
		},

		angularAria: {
			deps: ['angular', 'ngAnimate']
		},

		angularMsg: {
			deps: ['angular', 'ngAnimate', 'angularAria']
		},

		angularMaterial: {
			deps: ['angular', 'ngAnimate', 'angularAria', 'angularMsg']
		},

		jqueryTemplate: {
			deps: ['jquery.min']
		},

		uiBlock: {
			deps: ['angular']
		},

		Restangular: {
			deps: ['angular', 'underscore', 'lodash']
		},

		chartjs: {
			deps: ['jquery.min'],
			exports: 'Chart'
		},

		pieceLabel: {
			deps: ['jquery.min', 'chartjs']
		},

		"domToPdf": {
			deps: ['es6Promise', 'angular', 'domToImg', 'pdfmake']
		},

		autoComplete: {
			deps: ['jquery.min']
		},

		underscore: {
			exports: '_'
		}

	}

});

require(['externalModules'], function() {
	require(["angular", "app/app"], function(angular) {
		angular.bootstrap(document.documentElement, ['tsApp'], {
			//Enabling strict DI to make sure that there are no issues with minifier
			strictDi: true
		});
	});
});
